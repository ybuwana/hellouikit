//
//  YBZViewController.m
//  HelloUIKit
//
//  Created by Yuki Buwana on 9/15/14.
//  Copyright (c) 2014 Yuki Buwana. All rights reserved.
//

#import "YBZViewController.h"

@interface YBZViewController ()

@end

@implementation YBZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [outletTextField setDelegate:self];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    // Dismiss the keyboard when the user click return
    [outletTextField resignFirstResponder];
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender
{
    // If the UITextField null then return
    if ([[outletTextField text] isEqualToString:@""]) {
        return;
    }
    
    [outletLabel setText:[outletTextField text]];
    
    // Dismiss the keyboard when the user click button
    [outletTextField resignFirstResponder];
}

- (IBAction)stepperPressed:(id)sender
{
    UIStepper *stepper = sender;
    
    double value = [stepper value];
    
    [outletLabel setText:[NSString stringWithFormat:@"%.0f", value]];
    
}

- (IBAction)sliderChanged:(id)sender
{
    UISlider *slider = sender;
    
    double value = [slider value];
    
    [outletLabel setText:[NSString stringWithFormat:@"%.2f", value]];
}

- (IBAction)segmentChanged:(id)sender
{
    UISegmentedControl *segment = sender;
    
    NSInteger value = segment.selectedSegmentIndex;
    
    [outletLabel setText:[NSString stringWithFormat:@"Selected Segment: %d", value]];
}

- (IBAction)switchChaned:(id)sender
{
    UISwitch *inputSwitch = sender;
    
    if (inputSwitch.on) {
        [outletActivity startAnimating];
    } else {
        [outletActivity stopAnimating];
    }
}

@end
