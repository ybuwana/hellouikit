//
//  YBZViewController.h
//  HelloUIKit
//
//  Created by Yuki Buwana on 9/15/14.
//  Copyright (c) 2014 Yuki Buwana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBZViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UILabel *outletLabel;
    IBOutlet UITextField *outletTextField;
    IBOutlet UIActivityIndicatorView *outletActivity;
}

- (IBAction)buttonPressed:(id)sender;
- (IBAction)stepperPressed:(id)sender;
- (IBAction)sliderChanged:(id)sender;
- (IBAction)segmentChanged:(id)sender;
- (IBAction)switchChaned:(id)sender;
@end
