//
//  main.m
//  HelloUIKit
//
//  Created by Yuki Buwana on 9/15/14.
//  Copyright (c) 2014 Yuki Buwana. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YBZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YBZAppDelegate class]));
    }
}
