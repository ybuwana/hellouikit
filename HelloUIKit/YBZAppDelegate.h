//
//  YBZAppDelegate.h
//  HelloUIKit
//
//  Created by Yuki Buwana on 9/15/14.
//  Copyright (c) 2014 Yuki Buwana. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YBZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
